From: Michael Biebl <biebl@debian.org>
Date: Thu, 18 Jul 2013 20:11:02 +0200
Subject: Use Debian specific config files

Use /etc/default/locale instead of /etc/locale.conf for locale settings.

Use /etc/default/keyboard instead of /etc/X11/xorg.conf.d/00-keyboard.conf for
keyboard configuration.

Read/write /etc/timezone if /etc/localtime does not exist.
---
 src/basic/time-util.c    |  21 ++++-
 src/core/locale-setup.c  |  22 +++++
 src/locale/keymap-util.c | 206 ++++++++++++++++++++++++-----------------------
 src/timedate/timedated.c |  10 +++
 4 files changed, 156 insertions(+), 103 deletions(-)

diff --git a/src/basic/time-util.c b/src/basic/time-util.c
index fe201c3..fdcdbda 100644
--- a/src/basic/time-util.c
+++ b/src/basic/time-util.c
@@ -1395,8 +1395,25 @@ int get_timezone(char **tz) {
         int r;
 
         r = readlink_malloc("/etc/localtime", &t);
-        if (r < 0)
-                return r; /* returns EINVAL if not a symlink */
+        if (r < 0) {
+                if (r != -EINVAL)
+                        return r; /* returns EINVAL if not a symlink */
+
+                r = read_one_line_file("/etc/timezone", &t);
+                if (r < 0) {
+                        if (r != -ENOENT)
+                                log_warning_errno(r, "Failed to read /etc/timezone: %m");
+                        return -EINVAL;
+                }
+
+                if (!timezone_is_valid(t, LOG_DEBUG))
+                        return -EINVAL;
+                z = strdup(t);
+                if (!z)
+                        return -ENOMEM;
+                *tz = z;
+                return 0;
+        }
 
         e = path_startswith(t, "/usr/share/zoneinfo/");
         if (!e)
diff --git a/src/core/locale-setup.c b/src/core/locale-setup.c
index c14523f..b35b8e6 100644
--- a/src/core/locale-setup.c
+++ b/src/core/locale-setup.c
@@ -64,6 +64,28 @@ int locale_setup(char ***environment) {
                         log_warning_errno(r, "Failed to read /etc/locale.conf: %m");
         }
 
+        if (r <= 0) {
+                r = parse_env_file(NULL, "/etc/default/locale", NEWLINE,
+                                   "LANG",              &variables[VARIABLE_LANG],
+                                   "LANGUAGE",          &variables[VARIABLE_LANGUAGE],
+                                   "LC_CTYPE",          &variables[VARIABLE_LC_CTYPE],
+                                   "LC_NUMERIC",        &variables[VARIABLE_LC_NUMERIC],
+                                   "LC_TIME",           &variables[VARIABLE_LC_TIME],
+                                   "LC_COLLATE",        &variables[VARIABLE_LC_COLLATE],
+                                   "LC_MONETARY",       &variables[VARIABLE_LC_MONETARY],
+                                   "LC_MESSAGES",       &variables[VARIABLE_LC_MESSAGES],
+                                   "LC_PAPER",          &variables[VARIABLE_LC_PAPER],
+                                   "LC_NAME",           &variables[VARIABLE_LC_NAME],
+                                   "LC_ADDRESS",        &variables[VARIABLE_LC_ADDRESS],
+                                   "LC_TELEPHONE",      &variables[VARIABLE_LC_TELEPHONE],
+                                   "LC_MEASUREMENT",    &variables[VARIABLE_LC_MEASUREMENT],
+                                   "LC_IDENTIFICATION", &variables[VARIABLE_LC_IDENTIFICATION],
+                                   NULL);
+
+                if (r < 0 && r != -ENOENT)
+                        log_warning_errno(r, "Failed to read /etc/default/locale: %m");
+        }
+
         add = NULL;
         for (i = 0; i < _VARIABLE_LC_MAX; i++) {
                 char *s;
diff --git a/src/locale/keymap-util.c b/src/locale/keymap-util.c
index 598b931..0f1aed4 100644
--- a/src/locale/keymap-util.c
+++ b/src/locale/keymap-util.c
@@ -85,6 +85,7 @@ void locale_simplify(char *locale[_VARIABLE_LC_MAX]) {
 int locale_read_data(Context *c, sd_bus_message *m) {
         struct stat st;
         int r;
+        const char *path = "/etc/locale.conf";
 
         /* Do not try to re-read the file within single bus operation. */
         if (m && m == c->locale_cache)
@@ -93,7 +94,11 @@ int locale_read_data(Context *c, sd_bus_message *m) {
         /* To suppress multiple call of stat(), store the message to cache here. */
         c->locale_cache = m;
 
-        r = stat("/etc/locale.conf", &st);
+        r = stat(path, &st);
+        if (r < 0 && errno == ENOENT) {
+                path = "/etc/default/locale";
+                r = stat(path, &st);
+        }
         if (r < 0 && errno != ENOENT)
                 return -errno;
 
@@ -108,7 +113,7 @@ int locale_read_data(Context *c, sd_bus_message *m) {
                 c->locale_mtime = t;
                 context_free_locale(c);
 
-                r = parse_env_file(NULL, "/etc/locale.conf", NEWLINE,
+                r = parse_env_file(NULL, path, NEWLINE,
                                    "LANG",              &c->locale[VARIABLE_LANG],
                                    "LANGUAGE",          &c->locale[VARIABLE_LANGUAGE],
                                    "LC_CTYPE",          &c->locale[VARIABLE_LC_CTYPE],
@@ -189,9 +194,6 @@ int vconsole_read_data(Context *c, sd_bus_message *m) {
 }
 
 int x11_read_data(Context *c, sd_bus_message *m) {
-        _cleanup_fclose_ FILE *f = NULL;
-        bool in_section = false;
-        char line[LINE_MAX];
         struct stat st;
         usec_t t;
         int r;
@@ -203,7 +205,7 @@ int x11_read_data(Context *c, sd_bus_message *m) {
         /* To suppress multiple call of stat(), store the message to cache here. */
         c->x11_cache = m;
 
-        if (stat("/etc/X11/xorg.conf.d/00-keyboard.conf", &st) < 0) {
+        if (stat("/etc/default/keyboard", &st) < 0) {
                 if (errno != ENOENT)
                         return -errno;
 
@@ -220,56 +222,15 @@ int x11_read_data(Context *c, sd_bus_message *m) {
         c->x11_mtime = t;
         context_free_x11(c);
 
-        f = fopen("/etc/X11/xorg.conf.d/00-keyboard.conf", "re");
-        if (!f)
-                return -errno;
-
-        while (fgets(line, sizeof(line), f)) {
-                char *l;
-
-                char_array_0(line);
-                l = strstrip(line);
-
-                if (IN_SET(l[0], 0, '#'))
-                        continue;
-
-                if (in_section && first_word(l, "Option")) {
-                        _cleanup_strv_free_ char **a = NULL;
-
-                        r = strv_split_extract(&a, l, WHITESPACE, EXTRACT_QUOTES);
-                        if (r < 0)
-                                return r;
-
-                        if (strv_length(a) == 3) {
-                                char **p = NULL;
-
-                                if (streq(a[1], "XkbLayout"))
-                                        p = &c->x11_layout;
-                                else if (streq(a[1], "XkbModel"))
-                                        p = &c->x11_model;
-                                else if (streq(a[1], "XkbVariant"))
-                                        p = &c->x11_variant;
-                                else if (streq(a[1], "XkbOptions"))
-                                        p = &c->x11_options;
-
-                                if (p) {
-                                        free_and_replace(*p, a[2]);
-                                }
-                        }
-
-                } else if (!in_section && first_word(l, "Section")) {
-                        _cleanup_strv_free_ char **a = NULL;
-
-                        r = strv_split_extract(&a, l, WHITESPACE, EXTRACT_QUOTES);
-                        if (r < 0)
-                                return -ENOMEM;
-
-                        if (strv_length(a) == 2 && streq(a[1], "InputClass"))
-                                in_section = true;
+        r = parse_env_file(NULL, "/etc/default/keyboard", NEWLINE,
+                           "XKBMODEL",          &c->x11_model,
+                           "XKBLAYOUT",         &c->x11_layout,
+                           "XKBVARIANT",        &c->x11_variant,
+                           "XKBOPTIONS",        &c->x11_options,
+                           NULL);
 
-                } else if (in_section && first_word(l, "EndSection"))
-                        in_section = false;
-        }
+        if (r < 0)
+                return r;
 
         return 0;
 }
@@ -278,9 +239,18 @@ int locale_write_data(Context *c, char ***settings) {
         _cleanup_strv_free_ char **l = NULL;
         struct stat st;
         int r, p;
+        const char *path = "/etc/locale.conf";
 
         /* Set values will be returned as strv in *settings on success. */
 
+        r = load_env_file(NULL, path, NULL, &l);
+        if (r < 0 && r == -ENOENT) {
+                path = "/etc/default/locale";
+                r = load_env_file(NULL, path, NULL, &l);
+        }
+        if (r < 0 && r != -ENOENT)
+                return r;
+
         for (p = 0; p < _VARIABLE_LC_MAX; p++) {
                 _cleanup_free_ char *t = NULL;
                 char **u;
@@ -303,20 +273,20 @@ int locale_write_data(Context *c, char ***settings) {
         }
 
         if (strv_isempty(l)) {
-                if (unlink("/etc/locale.conf") < 0)
+                if (unlink(path) < 0)
                         return errno == ENOENT ? 0 : -errno;
 
                 c->locale_mtime = USEC_INFINITY;
                 return 0;
         }
 
-        r = write_env_file_label("/etc/locale.conf", l);
+        r = write_env_file_label(path, l);
         if (r < 0)
                 return r;
 
         *settings = TAKE_PTR(l);
 
-        if (stat("/etc/locale.conf", &st) >= 0)
+        if (stat(path, &st) >= 0)
                 c->locale_mtime = timespec_load(&st.st_mtim);
 
         return 0;
@@ -384,70 +354,104 @@ int vconsole_write_data(Context *c) {
 }
 
 int x11_write_data(Context *c) {
-        _cleanup_fclose_ FILE *f = NULL;
-        _cleanup_free_ char *temp_path = NULL;
         struct stat st;
         int r;
+        char *t, **u, **l = NULL;
 
-        if (isempty(c->x11_layout) &&
-            isempty(c->x11_model) &&
-            isempty(c->x11_variant) &&
-            isempty(c->x11_options)) {
+        r = load_env_file(NULL, "/etc/default/keyboard", NULL, &l);
+        if (r < 0 && r != -ENOENT)
+                return r;
 
-                if (unlink("/etc/X11/xorg.conf.d/00-keyboard.conf") < 0)
-                        return errno == ENOENT ? 0 : -errno;
+        /* This could perhaps be done more elegantly using an array
+         * like we do for the locale, instead of struct
+         */
+        if (isempty(c->x11_layout)) {
+                l = strv_env_unset(l, "XKBLAYOUT");
+        } else {
+                if (asprintf(&t, "XKBLAYOUT=%s", c->x11_layout) < 0) {
+                        strv_free(l);
+                        return -ENOMEM;
+                }
 
-                c->vc_mtime = USEC_INFINITY;
-                return 0;
+                u = strv_env_set(l, t);
+                free(t);
+                strv_free(l);
+
+                if (!u)
+                        return -ENOMEM;
+
+                l = u;
         }
 
-        mkdir_p_label("/etc/X11/xorg.conf.d", 0755);
+        if (isempty(c->x11_model)) {
+                l = strv_env_unset(l, "XKBMODEL");
+        } else {
+                if (asprintf(&t, "XKBMODEL=%s", c->x11_model) < 0) {
+                        strv_free(l);
+                        return -ENOMEM;
+                }
 
-        r = fopen_temporary("/etc/X11/xorg.conf.d/00-keyboard.conf", &f, &temp_path);
-        if (r < 0)
-                return r;
+                u = strv_env_set(l, t);
+                free(t);
+                strv_free(l);
 
-        (void) __fsetlocking(f, FSETLOCKING_BYCALLER);
-        (void) fchmod(fileno(f), 0644);
+                if (!u)
+                        return -ENOMEM;
 
-        fputs("# Written by systemd-localed(8), read by systemd-localed and Xorg. It's\n"
-              "# probably wise not to edit this file manually. Use localectl(1) to\n"
-              "# instruct systemd-localed to update it.\n"
-              "Section \"InputClass\"\n"
-              "        Identifier \"system-keyboard\"\n"
-              "        MatchIsKeyboard \"on\"\n", f);
+                l = u;
+        }
 
-        if (!isempty(c->x11_layout))
-                fprintf(f, "        Option \"XkbLayout\" \"%s\"\n", c->x11_layout);
+        if (isempty(c->x11_variant)) {
+                l = strv_env_unset(l, "XKBVARIANT");
+        } else {
+                if (asprintf(&t, "XKBVARIANT=%s", c->x11_variant) < 0) {
+                        strv_free(l);
+                        return -ENOMEM;
+                }
 
-        if (!isempty(c->x11_model))
-                fprintf(f, "        Option \"XkbModel\" \"%s\"\n", c->x11_model);
+                u = strv_env_set(l, t);
+                free(t);
+                strv_free(l);
 
-        if (!isempty(c->x11_variant))
-                fprintf(f, "        Option \"XkbVariant\" \"%s\"\n", c->x11_variant);
+                if (!u)
+                        return -ENOMEM;
+
+                l = u;
+        }
 
-        if (!isempty(c->x11_options))
-                fprintf(f, "        Option \"XkbOptions\" \"%s\"\n", c->x11_options);
+        if (isempty(c->x11_options)) {
+                l = strv_env_unset(l, "XKBOPTIONS");
+        } else {
+                if (asprintf(&t, "XKBOPTIONS=%s", c->x11_options) < 0) {
+                        strv_free(l);
+                        return -ENOMEM;
+                }
 
-        fputs("EndSection\n", f);
+                u = strv_env_set(l, t);
+                free(t);
+                strv_free(l);
 
-        r = fflush_sync_and_check(f);
-        if (r < 0)
-                goto fail;
+                if (!u)
+                        return -ENOMEM;
 
-        if (rename(temp_path, "/etc/X11/xorg.conf.d/00-keyboard.conf") < 0) {
-                r = -errno;
-                goto fail;
+                l = u;
         }
 
-        if (stat("/etc/X11/xorg.conf.d/00-keyboard.conf", &st) >= 0)
-                c->x11_mtime = timespec_load(&st.st_mtim);
+        if (strv_isempty(l)) {
+                strv_free(l);
 
-        return 0;
+                if (unlink("/etc/default/keyboard") < 0)
+                        return errno == ENOENT ? 0 : -errno;
+
+                c->vc_mtime = USEC_INFINITY;
+                return 0;
+        }
 
-fail:
-        if (temp_path)
-                (void) unlink(temp_path);
+        r = write_env_file("/etc/default/keyboard", l);
+        strv_free(l);
+
+        if (r >= 0 && stat("/etc/default/keyboard", &st) >= 0)
+                c->x11_mtime = timespec_load(&st.st_mtim);
 
         return r;
 }
diff --git a/src/timedate/timedated.c b/src/timedate/timedated.c
index 3b70939..cffaf07 100644
--- a/src/timedate/timedated.c
+++ b/src/timedate/timedated.c
@@ -211,6 +211,7 @@ static int context_read_data(Context *c) {
 static int context_write_data_timezone(Context *c) {
         _cleanup_free_ char *p = NULL;
         int r = 0;
+        struct stat st;
 
         assert(c);
 
@@ -218,6 +219,9 @@ static int context_write_data_timezone(Context *c) {
                 if (unlink("/etc/localtime") < 0 && errno != ENOENT)
                         r = -errno;
 
+                if (unlink("/etc/timezone") < 0 && errno != ENOENT)
+                        r = -errno;
+
                 return r;
         }
 
@@ -229,6 +233,12 @@ static int context_write_data_timezone(Context *c) {
         if (r < 0)
                 return r;
 
+        if (stat("/etc/timezone", &st) == 0 && S_ISREG(st.st_mode)) {
+                r = write_string_file("/etc/timezone", c->zone, WRITE_STRING_FILE_CREATE|WRITE_STRING_FILE_ATOMIC);
+                if (r < 0)
+                        return r;
+        }
+
         return 0;
 }
 
